#!/bin/bash
# Version: 1.0.0  
# Author: seven
# Date: 2015/1/5  
# Description: Php Install Script


lamp_dir=$(cd `dirname $0`; pwd)

rm -rf /sk/server/php/

yum -y install libXpm-devel

#install php
    tar -zxvf $lamp_dir/tools/php/php-5.5.12.tar.gz -C $lamp_dir/tools/php | tee /sk/server/log/install/php/php_tar.log
    cd $lamp_dir/tools/php/php-5.5.12/
    ./configure --prefix=/sk/server/php \
    --with-xpm-dir=/usr/lib64 \
    --with-config-file-path=/sk/server/php/etc/ \
    --with-apxs2=/sk/server/apache/bin/apxs \
    --with-zlib-dir \
    --with-jpeg-dir=/sk/server/jpeg/ \
    --with-gd=/sk/server/gd2/ \
    --with-png-dir=/sk/server/libpng \
    --with-freetype-dir=/sk/server/freetype/ \
    --enable-mbregex --enable-bcmath \
    --with-libxml-dir=/usr \
    --enable-mbstring=all --with-pdo-mysql \
    --with-mysqli=/sk/server/mysql/bin/mysql_config \
    --with-mysql=/sk/server/mysql/ \
    --enable-soap \
    --enable-sockets \
    --with-openssl \
    --with-curl | tee /sk/server/log/install/php/php_configure.log
    make && make install | tee /sk/server/log/install/php/php_install.log


\cp -f $lamp_dir/tools/php/php-5.5.12/php.ini-production /sk/server/php/etc/php.ini

echo "###### php install completed ######"



